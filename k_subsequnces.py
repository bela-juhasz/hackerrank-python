
from itertools import combinations, chain
import unittest

#  [ x for x in range(20) if x % 2 == 0]


def powertools(s):
    #s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(len(s) + 1))


def getAllWindows(L):
    for w in range(1, len(L)+1):
        for i in range(len(L)-w+1):
            yield L[i:i+w]


def kSub(k, nums):
    #return len([p for p in list(permutations(nums)) if sum(p) % k == 0])
    #return len([p for p in list(powertools(nums)) if sum(p) % k == 0])
    #return [sum(p) for p in list(powertools(nums)) if ((sum(p) % k) == 0)]
    return len([p for p in getAllWindows(nums) if ((sum(p) % k) == 0)])


# https://docs.python.org/3/library/unittest.html#assert-methods
class TestStringMethods(unittest.TestCase):

    def test_upper(self):
        self.assertEqual(kSub(3, [1, 2, 3, 4, 1]), 4)